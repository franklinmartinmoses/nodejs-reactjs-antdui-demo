# To run this website

#Pre-requisites

Install Node JS
https://nodejs.org/en/

Run the below command in Window CMD terminal or git bash

#Project setup

`npm install`
`export PORT=8080`

Above command starts the node server

open new terminal

`cd www/app`
`npm install`
`gulp build` or `gulp default` to see your changes reflected without re-build
`cd ../../`
`npm start`

#Database details



This project is using google Cloud platform.

##Create cloud SQL instance
https://cloud.google.com/sql/docs/mysql/quickstart

`CREATE DATABASE jobportal`
`USE jobportal;`

`CREATE TABLE jobdetails (title VARCHAR(255), type VARCHAR(255), skills VARCHAR(255), rate INT(5), experience VARCHAR(255),location VARCHAR(255), description VARCHAR(255), tag VARCHAR(255), replyrate VARCHAR(255),
    entryID INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(entryID));`

You can use sample entries by exporting sql-table.csv included in the repository.

##Install the Cloud SQL Proxy client on your local machine
https://cloud.google.com/sql/docs/mysql/quickstart-proxy-test

##Setting up database config details as environment variable

`export DATABASE_USER=YOUR_DATABASE_USER`
`export DATABASE_PASSWORD=YOUR_DATABASE_PASS`
`export DATABASE_NAME=jobportal`
`export TABLE_NAME=jobdetails`

Open the below url in browser

`http://localhost:8080`


#Project hosting

This project is hosted in google cloud platform. Verified in the below url
http://fluid-kiln-216901.appspot.com/
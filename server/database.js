const Knex = require('knex');

let connect = function(){
  const config = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
  };

  /*
  user: process.env.DATABASE_USER || 'root',
    password: process.env.DATABASE_PASSWORD || 'franklin_777',
    database: process.env.DATABASE_NAME || 'jobportal'

  */

  if (process.env.INSTANCE_CONNECTION_NAME && process.env.NODE_ENV === 'production') {
    config.socketPath = `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`;
  }

  // Connect to the database
  const knex = Knex({
    client: 'mysql',
    connection: config
  });
  console.log('SQL connection successful')
  return knex;
};

module.exports = connect;
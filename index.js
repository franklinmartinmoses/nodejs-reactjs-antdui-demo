var createError = require('http-errors');
var express = require('express');
var path = require('path');
// var connection = require('./server/database.js');
const knex = require('./server/database.js')();
var app = express();

app.use(express.static(path.join(__dirname, '/www/app/dist')));

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/www/app/dist/index.html'));
});
var tableName = process.env.TABLE_NAME;
console.log(tableName)
app.route('/api/'+tableName)
  .get(function(req, res, next) {
    knex.select('*')
    .from('jobdetails')
    .then((results) => {
    	console.log('fetching details')
    	res.json(results);
      //return results.map((visit) => `Time: ${visit.timestamp}, AddrHash: ${visit.userIp}`);
    });
  });

app.listen(process.env.PORT);
console.log('Listening port '+process.env.PORT)
import React from 'react';
import { Menu } from 'antd';
import SearchComp from './searchComp.js';
import PageLayout from './pageLayout.js';
class App extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {listData: [],filteredData: []};
  	}
	componentDidMount() {
	    fetch(`api/jobdetails`)
			.then(response => {		
			        return response.json();
			})
			.then(json => {
				const listData = [];
				for (let i = 0; i < json.length; i++) {
					var tag = json[i].skills.split(',');
					json[i].tag = tag;
			  		listData.push(json[i]);
				}
			    this.setState({
			    	listData,
			    	filteredData: listData});
				})
  	}
  	filterList = (type, value) => {
  		let filteredData = this.state.listData
  		if(value && value.length > 0){
  			filteredData = filteredData.filter((item) => {
  				if(typeof(value) === 'Array' || typeof(value) === 'object'){
  					for(var i=0; i< value.length;i++){
  						return item[type] && item[type].toLowerCase().indexOf(value[i].toLowerCase()) >= 0
  					}
  				}else{
  					return item[type] && item[type].toLowerCase().indexOf(value.toLowerCase()) >= 0
  				}
	  			
	  		})	
  		}
  		
  		this.setState({
  			filteredData
  		})
  	}
	render() {
	  return (
	     <div>
	        <Header/>
	        <Content listData={this.state.filteredData} filterList={this.filterList} />
	     </div>
	  );
	}
}
class Header extends React.Component {
   render() {
      return (
         <nav className="navbar-main">
         	<div className="container">
         		<div className="logo" />
         		<Menu
			        theme="light"
			        mode="horizontal"
			        style={{ lineHeight: '69px' }}
			      >
		        <Menu.Item key="1">How it works</Menu.Item>
		        <Menu.Item key="2">Browse</Menu.Item>
		        <Menu.Item key="3">Search</Menu.Item>
		        <Menu.Item key="4">My Account</Menu.Item>
		      </Menu>
         	</div>
         </nav>
      );
   }
}
class Content extends React.Component {
   render() {
      return (
         <div className="pageWrapper">
         	<div className="container">
         		<SearchComp filterList={this.props.filterList}></SearchComp>
            	<PageLayout listData={this.props.listData} filterList={this.props.filterList}></PageLayout>
         	</div>            
         </div>
      );
   }
}
export default App;
import React from 'react';
import { Row, Col, List, Avatar, Icon, Badge, Tag } from 'antd';

const TitleElem = ({item}) => (
	<Row className="titleSection">
		<Col span={20}>
			<a href={item.href} style={{display:'inline-block'}}>{item.title}</a>
			<Badge count={item.type} className={item.type} style={{display:'inline-block',marginLeft:'10px'}} />
			
		</Col>
		<Col span={4}>
			<h4>${item.rate}/hr</h4>			
		</Col>
		<Col className="subSection">
			<div className="subItem">
				<Icon className="subTitle" type="reconciliation" theme="outlined" /> <span className="subTitle coderType">Epic Coders</span>
			</div>
			<div className="subItem">
				<Icon className="subTitle" type="environment" theme="filled" /> <span className="subTitle">{item.location}</span>
			</div>
			
		</Col>
		<Col className="subSection">
			<div className="subItem">
				<span className="subTitle ">Reply rate: {item.rate}</span>
			</div>
		</Col>
	</Row>
	
)

function TagElem(tags) {
	var tagElem = [];
	for(var i=0; i< tags.length; i++){
		tagElem.push(<Tag>{tags[i]}</Tag>);
	}
	return tagElem;
}

class SearchResult extends React.Component{
	render(){
		return (
			<div className="contentSection">
				<List
				itemLayout="vertical"
				size="large"
				pagination={{
				  onChange: (page) => {
				    console.log(page);
				  },
				  pageSize: 5,
				}}
				dataSource={this.props.listData}
				renderItem={item => (
				  <List.Item
				    key={item.title}
				    actions={TagElem(item.tag)}				  >
				    <List.Item.Meta
				      avatar={<Avatar src={item.avatar} />}
				      title={<TitleElem item={item} />}
				      description={item.description}
				    />
				    {item.content}
				  </List.Item>
				)}
				/>
			</div>
		)
	}
}
export default SearchResult
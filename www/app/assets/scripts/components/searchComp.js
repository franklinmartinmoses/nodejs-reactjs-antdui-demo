import React from 'react';
import { Input } from 'antd';
const Search = Input.Search;
class SearchComp extends React.Component{
   onSearch = (type, value) => { 
     this.props.filterList(type,value);
   }
   handleChange = (type, event) => { 
     this.props.filterList(type,event.target.value);
   }
	render(){
		return (
				<Search
			      placeholder="Search by keywords (PHP, .NET, graphic design, etc.)"
			      enterButton="Search"
			      size="large"
			      className="searchComp"
               onSearch={this.onSearch.bind(this, 'title')}
               onChange={this.handleChange.bind(this, 'title')}
			    />
		)
	}
}
export default SearchComp
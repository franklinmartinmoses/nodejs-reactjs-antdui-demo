import React from 'react';
import { Row, Col, Select, Checkbox, Icon, Slider, Input, InputNumber  } from 'antd';
const Option = Select.Option;
const InputGroup = Input.Group;
const optionList = ['reactjs', '.net', 'mobile','ios', 'android','PHP','Javascript'] 


class FilterComp extends React.Component{
	state = { 
		options: optionList, 
		children:[], 
		selectedItems:[],
		minRange: 1,
		maxRange: 40,
		filterValue:[]
	};
	handleMultiSelect = (type, value) => { 
	  this.props.filterList(type,value);
	}
	handleChange = (type, value) => {	  
	  this.props.filterList(type,value);
	}
	eventChange = (type, event) => {
	  if(event.target.checked){
	  	this.state.filterValue.push(event.target.value);  
	  }else{
	  	var i = this.state.filterValue.indexOf(event.target.value);
		if(i != -1) {
			this.state.filterValue.splice(i, 1);
		}
	  }
	  
	  this.props.filterList(type,this.state.filterValue);
	}
	handleReset = () => {
		this.setState({
			selectedItems:[]
		})
	}
	componentDidMount() {
      for(let i=0; i< this.state.options.length; i++){
      	this.state.children.push(<Option key={this.state.options[i]}>{this.state.options[i]}</Option>)
      }
   	}
	render(){
		return (
			<div className="filterLabel">
				<Row >
		      		<Col span={4}><h5>Skills</h5></Col>
      				<Col span={8} offset={12}><a href="#" onClick={this.handleReset}>Clear</a></Col>      				
			    </Row>
			    <Row className="filterComponents">
			    	<Select
					    mode="multiple"
					    style={{ width: '100%' }}
					    placeholder="Select Skills"
					    onChange={this.handleMultiSelect.bind(this, 'skills')}
					    selectedItems={this.state.selectedItems}
					    allowClear={true}			    
					  >
					    {this.state.children}
				  </Select>
			    </Row>
			    <Row >
		      		<Col span={12}><h5>Availability</h5><Icon type="info-circle" theme="outlined" /></Col>
      				<Col span={4} offset={8}><a href="#" >Clear</a></Col>      				
			    </Row>
			    <Row className="filterComponents">
			    	<Col><Checkbox value="hourly" onChange={this.eventChange.bind(this, 'type')}>Hourly</Checkbox></Col>
		      		
		      		<Col><Checkbox value="part-time" onChange={this.eventChange.bind(this, 'type')}>Part-time (20 hrs/wk)</Checkbox></Col>
		      		<Col><Checkbox value="full-time" onChange={this.eventChange.bind(this, 'type')}>Full-time (40 hrs/wk)</Checkbox></Col>
			    </Row>
			    <Row >
		      		<Col span={12}><h5>Job type</h5><Icon type="info-circle" theme="outlined" /></Col>
      				<Col span={4} offset={8}><a href="#" >Clear</a></Col>      				
			    </Row>
			    <Row className="filterComponents">
			    	<Select placeholder="Select Job type" onChange={this.handleChange.bind(this, 'type')} style={{ width: '100%' }} >
				      <Option value="part-time">Part Time</Option>
				      <Option value="full-time">Full Time</Option>
				      <Option value="hourly">Hourly</Option>
				    </Select>
			    </Row>
			    <Row >
		      		<Col span={12}><h5>Pay rate / hr ($) </h5><Icon type="info-circle" theme="outlined" /></Col>
      				<Col span={4} offset={8}><a href="#" >Clear</a></Col>      				
			    </Row>
			    <Row className="filterComponents">
			    	<InputGroup size="small">
			          <Col span={4}>
			            <Input defaultValue="18" />
			          </Col>
			          <Col span={2}>
			            <span style={{textAlign:'center'}}>-</span>
			          </Col>
			          <Col span={4}>
			            <Input defaultValue="32" />
			          </Col>
			        </InputGroup>
			        <Slider range defaultValue={[18, 32]} autoFocus={true} />
			    </Row>
			    <Row >
		      		<Col span={12}><h5>Experience Level</h5></Col>
      				<Col span={4} offset={8}><a href="#" >Clear</a></Col>      				
			    </Row>
			    <Row className="filterComponents">
			    	<Select placeholder="Select your experience level" style={{ width: '100%' }} onChange={this.handleChange.bind(this, 'experience')} >
				      <Option value="1to3">1-3 year</Option>
				      <Option value="4to6">4-6 year</Option>
				      <Option value="above7">Above 7</Option>
				    </Select>
			    </Row>
			    <Row >
		      		<Col span={12}><h5>Countries</h5></Col>
      				<Col span={4} offset={8}><a href="#" >Clear</a></Col>      				
			    </Row>
			    <Row className="filterComponents">
			    	<Select
					    showSearch
					    style={{ width: '100%' }}
					    placeholder="Select a person"
					    onChange={this.handleChange.bind(this, 'location')}
					    optionFilterProp="children"
					    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
					  >
					    <Option value="UK">United Kingdom</Option>
					    <Option value="IN">India</Option>
					    <Option value="USA">United State of America</Option>
					    <Option value="UAE">United Arab Emirates</Option>
				  </Select>
			    </Row>
		    </div>		      
		)
	}
}
export default FilterComp
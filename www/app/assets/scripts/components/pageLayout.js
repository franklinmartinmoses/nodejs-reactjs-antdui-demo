import React from 'react';
import { Row, Col, Icon, Button } from 'antd';
import FilterComp from './filterComponent.js';
import SearchResult from './searchResults.js';

class SearchComp extends React.Component{
	render(){
		return (
			<Row>
		      <Col span={6} className="filterWrapper">
		      	<Row className="filterSubtitle">
		      		<Col span={4}><h5>FILTERS</h5></Col>
      				<Col span={8} offset={12}><a href="#">Clear all filters</a></Col>
		      	</Row>
		      	<FilterComp filterList={this.props.filterList}></FilterComp>
		      </Col>
		      <Col span={12} className="jobListWrapper">		      	
		      		<SearchResult listData={this.props.listData}></SearchResult>
		      </Col>
		      <Col span={6} className="infoWrapper">
		      	<div className="signUp">
		      		<Col><Icon type="deployment-unit" theme="outlined" /></Col>
		      		<Col><h2>Track time on Hubstaff</h2></Col>
		      		<Col><span>Pay only for the hours worked</span></Col>
		      		<Col><Button type="primary" size="large">Sign Up</Button></Col>
		      		<Col><a href="#" >Learn more...</a></Col>
		      	</div>
		      	<div className="portalPicks">
		      		<Row>
			      		<Col><div className="title"><h3>TOP JOBS</h3></div></Col>
			      		<Col span={20} ><h4>Senior Ruby on Rails</h4></Col>
			      		<Col span={4} ><h4>$60/hr</h4></Col>
			      		<Col><div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div></Col>
			      		<Col span={20} ><h4>Senior Ruby on Rails</h4></Col>
			      		<Col span={4} ><h4>$60/hr</h4></Col>
			      		<Col><div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div></Col>
		      		</Row>
		      	</div>
		      	<div className="portalPicks">
		      		<Row>
			      		<Col><div className="title"><h3>Mostly viewed</h3></div></Col>
			      		<Col span={20} ><h4>Senior Ruby on Rails</h4></Col>
			      		<Col span={4} ><h4>$60/hr</h4></Col>
			      		<Col><div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div></Col>
			      		<Col span={20} ><h4>Senior Ruby on Rails</h4></Col>
			      		<Col span={4} ><h4>$60/hr</h4></Col>
			      		<Col><div className="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</div></Col>
		      		</Row>
		      	</div>
		      </Col>
		    </Row>
		)
	}
}
export default SearchComp
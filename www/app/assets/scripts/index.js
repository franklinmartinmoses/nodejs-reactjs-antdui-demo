import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/appWrapper.js';
ReactDOM.render(
  <App />,
  document.getElementById('app')
);
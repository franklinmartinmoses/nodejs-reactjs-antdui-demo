var gulp         = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var babel        = require('gulp-babel');
var browserSync  = require('browser-sync');
var concat       = require('gulp-concat');
var eslint       = require('gulp-eslint');
var filter       = require('gulp-filter');
var newer        = require('gulp-newer');
var notify       = require('gulp-notify');
var plumber      = require('gulp-plumber');
var reload       = browserSync.reload;
var less         = require('gulp-less');
var sourcemaps   = require('gulp-sourcemaps');
var webpack = require('gulp-webpack');
var path = require('path');

var onError = function(err) {
  notify.onError({
    title:    "Error",
    message:  "<%= error %>",
  })(err);
  this.emit('end');
};

var plumberOptions = {
  errorHandler: onError,
};

var jsFiles = {
  vendor: [

  ],
  source: [
    'assets/scripts/index.js'
  ]
};

// Lint JS/JSX files
gulp.task('eslint', function() {
  return gulp.src(jsFiles.source)
    .pipe(eslint({
      baseConfig: {
        "ecmaFeatures": {
           "jsx": true
         }
      }
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

// Copy react.js and react-dom.js to assets/js/src/vendor
// only if the copy in node_modules is "newer"
gulp.task('copy-react', function() {
  return gulp.src('node_modules/react/dist/react.js')
    .pipe(newer('assets/js/src/vendor/react.js'))
    .pipe(gulp.dest('assets/js/src/vendor'));
});
gulp.task('copy-react-dom', function() {
  return gulp.src('node_modules/react-dom/dist/react-dom.js')
    .pipe(newer('assets/js/src/vendor/react-dom.js'))
    .pipe(gulp.dest('assets/js/src/vendor'));
});

// Copy assets/js/vendor/* to assets/js
gulp.task('copy-js-vendor', function() {
  return gulp
    .src([
      'node_modules/react/dist/react.js',
      'node_modules/react-dom/dist/react-dom.js'
    ])
    .pipe(gulp.dest('dist/assets/scripts/vendor'));
});

// Concatenate jsFiles.vendor and jsFiles.source into one JS file.
// Run copy-react and eslint before concatenating
gulp.task('concat', ['copy-react', 'copy-react-dom'], function() {
  return gulp.src(jsFiles.vendor.concat(jsFiles.source))
    .pipe(sourcemaps.init())
    .pipe(babel({
      only: [
        'assets/scripts',
      ],
      compact: false
    }))
    .pipe(concat('index.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist/assets/scripts'));
});

// Compile javascript files
var BUILD_DIR = path.resolve(__dirname, 'dist/assets/scripts');
var APP_DIR = path.resolve(__dirname, 'assets/scripts');
gulp.task('webpack', function() {
    return gulp.src('assets/scripts/index.js')
        .pipe(webpack({
            module: {
                loaders: [{
                    test: /.jsx?$/,
                    loader: 'babel',
                    exclude: /node_modules/,
                    query: {
                        presets: ['es2015', 'react'],
                        plugins: ["transform-class-properties"]
                    },
                    options: {"plugins": ["transform-class-properties"]}
                }]
            },
            output: {
                filename: 'index.js'
            }
        }))
        .pipe(gulp.dest('./dist/assets/scripts'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Compile less to CSS
gulp.task('less', function() {
  return gulp.src('assets/styles/**/*.less')
    .pipe(less({javascriptEnabled: true}))
    .pipe(gulp.dest('dist/assets/styles'))
});

gulp.task('template',function(){
	return gulp.src('index.html')
			.pipe(gulp.dest('dist'))
			.pipe(browserSync.reload({
	            stream: true
	        }))
});

gulp.task('images', function() {
    return gulp.src('assets/images/**/*.*')
        .pipe(gulp.dest('dist/assets/images'))
        .pipe(browserSync.reload({
            stream: true
        }))
});
// Watch JS/JSX and Sass files
gulp.task('watch', function() {
  gulp.watch('assets/scripts/**/*.{js,jsx}', ['webpack']);
  gulp.watch('assets/styles/**/*.less', ['less']);
});

// BrowserSync
gulp.task('browsersync', function() {
  browserSync({
    server: {
      baseDir: './'
    },
    open: false,
    online: false,
    notify: false,
  });
});

gulp.task('build', ['images','less', 'copy-js-vendor', 'webpack','template']);
gulp.task('default', ['build', 'browsersync', 'watch', 'template']);